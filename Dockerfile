ARG NODE_VERSION

FROM node:${NODE_VERSION}

ARG ELK_VERSION

RUN mkdir /kibana && chown node:node /kibana
RUN mkdir /.yarn && chown node:node /.yarn

USER node

RUN git clone https://github.com/elastic/kibana.git --depth 1 --branch v${ELK_VERSION} /kibana

WORKDIR /kibana

RUN yarn kbn bootstrap

EXPOSE 5601
CMD ["bash", "-c", "yarn kbn bootstrap && node --trace-warnings --throw-deprecation --max_old_space_size=4096 scripts/kibana --dev --elasticsearch http://elasticsearch:9200 --port 5601 --host 0.0.0.0"]
